﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ScrollScript : MonoBehaviour
{
	public void Start()
	{
		transform.DOMoveX (transform.position.x - 40f, 2f, false).SetEase (Ease.Linear).SetLoops(-1,LoopType.Restart);
	}

} // Fin du script
