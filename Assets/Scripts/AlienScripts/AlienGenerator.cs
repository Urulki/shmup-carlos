﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienGenerator : MonoBehaviour
{

	public float frequency;
	public GameObject[] aliens;

	void Start ()
	{
		StartCoroutine(GenerateWave ());
	}

	IEnumerator GenerateWave()
	{
		foreach (GameObject alien in aliens)
		{
			yield return new WaitForSeconds (1f/frequency);
			Instantiate (alien, transform.position, transform.rotation);
		}

		Destroy (this.gameObject);

	}
	

}
