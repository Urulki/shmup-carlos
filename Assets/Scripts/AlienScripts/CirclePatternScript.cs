﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CirclePatternScript : MonoBehaviour {
    float timeCount = 0;
    float x;
    float y;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
        timeCount += Time.deltaTime;
        x = Mathf.Cos(timeCount*5);
        y = Mathf.Sin(timeCount*5);
        Circle();
    
    }

    void Circle()
    {
        transform.localPosition = new Vector3(2*x,2 * y, 0f);
    }
    
}
