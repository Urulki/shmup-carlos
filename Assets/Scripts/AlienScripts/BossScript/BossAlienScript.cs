﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BossAlienScript : MonoBehaviour {
    Rigidbody rb;
    public GameObject missileSalve1;
    public GameObject missileSalve2;
    GameObject missileSalve1Instance;
    GameObject missileSalve2Instance;
    void Start () {
        rb = GetComponent<Rigidbody>();
        Entrance();
        InvokeRepeating("Tirer1", 0.5f,2f);
        InvokeRepeating("Tirer2", 1.5f,2f);
    }
    void Entrance()
    {
        rb.DOMoveX(13f, 10f / 1f, false).SetEase(Ease.Linear);
    }
    void Tirer1()
    {
        missileSalve1Instance = (GameObject)Instantiate(missileSalve1,transform.position,transform.rotation); // On créé un missile partant tout droit
        Destroy(missileSalve1Instance, 5f);
    }
    void Tirer2()
    {
        missileSalve2Instance = (GameObject)Instantiate(missileSalve2, transform.position, transform.rotation); // On créé un missile partant tout droit
        Destroy(missileSalve2Instance, 5f);
    }
}
