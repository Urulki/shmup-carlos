﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienShotScript : MonoBehaviour {

    public GameObject missile;
    GameObject missileInstance;
    private void Start()
    {
        InvokeRepeating("Tirer", 0.5f, 3f);
    }

    void Tirer()
    {
        missileInstance = (GameObject)Instantiate(missile, transform.position, transform.rotation); // On créé un missile partant tout droit
        Destroy(missileInstance, 2.5f);
    }
}
