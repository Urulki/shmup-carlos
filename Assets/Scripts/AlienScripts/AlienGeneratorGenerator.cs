﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienGeneratorGenerator : MonoBehaviour
{

	public Generator[] alienGenerators;

	void Start ()
	{
		foreach (Generator generator in alienGenerators)
		{
			generator.generator.gameObject.SetActive (false);
		}

		StartCoroutine (GenerateGenerators ());
	}
	
	IEnumerator GenerateGenerators ()
	{
		foreach (Generator generator in alienGenerators)
		{
			yield return new WaitForSeconds(generator.waitFor);
			generator.generator.gameObject.SetActive (true);

		}
	}
} // Fin du script



[System.Serializable]
public class Generator
{
	public AlienGenerator generator;
	public float waitFor;
}


