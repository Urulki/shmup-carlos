﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MissileScript : MonoBehaviour
{

	public Rigidbody missileRb;
	public float vitesse;
    public float damage;
	public GameObject explosion;
    /*public AudioClip invaderKilled;
	public AudioClip playerKilled;*/


    void Start ()
	{
		missileRb.AddForce (transform.right * vitesse, ForceMode.VelocityChange);
	}


	void OnTriggerEnter(Collider objetTouche)
	{
		if (vitesse > 0 && objetTouche.tag == "Alien") 
		{
            Instantiate (explosion, objetTouche.transform.position, transform.rotation);
            Destroy (this.gameObject);
            objetTouche.SendMessageUpwards("ToucheMissile", damage, SendMessageOptions.DontRequireReceiver);
        }

		if (vitesse < 0 && objetTouche.tag == "Player") 
		{
			Instantiate (explosion, objetTouche.transform.position, transform.rotation);
            objetTouche.SendMessageUpwards("ToucheMissile", damage, SendMessageOptions.DontRequireReceiver);
            Destroy (this.gameObject); 
		}
			

	}
} 