﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageScript : MonoBehaviour {

    public int maxLife;
    [HideInInspector] public int currentLife;
    public int points;

    void Start()
    {
        if (maxLife < 1)
            maxLife = 1;

        currentLife = maxLife;
    }



    void ToucheMissile(int damage)
    {
        currentLife -= damage;

        if (currentLife < 1)
        {
            if (this.gameObject.tag == "Alien")
            {
                //ScoreScript.AddScore(points);
            }
            else if (gameObject.tag == "Player")
            {
                //ScoreScript.RemoveScore(points);
            }
            Destroy(this.gameObject);
        }
    }
}
