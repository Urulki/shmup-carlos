﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipScript : MonoBehaviour {

	public Rigidbody vaisseauRb;
	public float vitesse;
	public GameObject missile;
    private GameObject missileInstance;
    private GameObject tripleInstance;
    public GameObject tripleMissile;
    int life;
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () 
	{
        life = this.GetComponent<DamageScript>().currentLife;
        Tirer ();
        if (life == 0) Destroy(this.gameObject);
	}

	void FixedUpdate () 
	{
		Mouvement();
	}

	void Tirer()
	{
		if (Input.GetButtonDown("Jump")){
		  missileInstance = (GameObject)  Instantiate (missile, transform.position, transform.rotation); // On créé un missile partant tout droit
            Destroy(missileInstance, 2.5f); 
        }
		if (Input.GetButtonDown("Fire1")){
			tripleInstance = (GameObject) Instantiate (tripleMissile, transform.position, transform.rotation); // On créé un GameObject lançant 3 missiles ayant des directions différentes
            Destroy(tripleInstance, 2.5f);
        }
	}

	void Mouvement()
	{
		Vector3 targetPosition = transform.position + Vector3.right * Input.GetAxis ("Horizontal") * vitesse * Time.deltaTime + Vector3.up * Input.GetAxis ("Vertical") * vitesse * Time.deltaTime;
		vaisseauRb.MovePosition (targetPosition);
	}
}

