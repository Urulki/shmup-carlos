﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallScript : MonoBehaviour {

    public GameObject wallCol;
    private void OnCollisionEnter(Collision passingEntity)
    {
        if (passingEntity.gameObject.tag == "Alien" || passingEntity.gameObject.tag == "Missile")
        {
            Physics.IgnoreCollision(passingEntity.collider, wallCol.GetComponent<Collider>());
        }
    }

}
